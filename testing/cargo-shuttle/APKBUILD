# Maintainer: Orhun Parmaksız <orhunparmaksiz@gmail.com>
pkgname=cargo-shuttle
pkgver=0.43.0
pkgrel=0
pkgdesc="Cargo command for the Shuttle platform"
url="https://github.com/shuttle-hq/shuttle"
license="Apache-2.0"
# s390x, ppc64le, riscv64: blocked by ring crate
arch="all !s390x !ppc64le !riscv64"
makedepends="cargo cargo-auditable"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/shuttle-hq/shuttle/archive/v$pkgver/shuttle-$pkgver.tar.gz"
builddir="$srcdir/shuttle-$pkgver"
# tests require git/submodules to be initialized
options="!check"

prepare() {
	default_prepare
	cargo fetch --locked
}

build() {
	cargo auditable build -p "$pkgname" --release --frozen
	mkdir -p completions/
	local compgen="target/release/$pkgname generate shell"
	$compgen bash >"completions/$pkgname.bash"
	$compgen fish >"completions/$pkgname.fish"
	$compgen zsh >"completions/_$pkgname"
	mkdir -p man/
	"target/release/$pkgname" generate manpage > "man/$pkgname.1"
}

package() {
	install -Dm755 "target/release/$pkgname" -t "$pkgdir/usr/bin/"
	install -Dm644 README.md -t "$pkgdir/usr/share/doc/$pkgname"
	install -Dm644 "completions/$pkgname.bash" "$pkgdir/usr/share/bash-completion/completions/$pkgname"
	install -Dm644 "completions/$pkgname.fish" -t "$pkgdir/usr/share/fish/vendor_completions.d"
	install -Dm644 "completions/_$pkgname" -t "$pkgdir/usr/share/zsh/site-functions"
	install -Dm 644 "man/$pkgname.1" -t "$pkgdir/usr/share/man/man1"
}

sha512sums="
0750bae9e0e6b3208924c27c16ab2bf0e8c64908d830e0e7f169601e608a8bf20e7ce83188bde474afa01eaa0e30edb437f28508a6e8af032216b46cec7a2f18  shuttle-0.43.0.tar.gz
"
