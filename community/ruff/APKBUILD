# Maintainer: Hugo Osvaldo Barrera <hugo@whynothugo.nl>
pkgname=ruff
pkgver=0.3.5
pkgrel=0
pkgdesc="Extremely fast Python linter"
url="https://github.com/astral-sh/ruff"
# s390x: nix crate
arch="all !s390x"
license="MIT"
makedepends="py3-gpep517 py3-maturin cargo py3-installer"
subpackages="
	$pkgname-pyc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/astral-sh/ruff/archive/v$pkgver/ruff-$pkgver.tar.gz"
# net: cargo
options="net"

export CARGO_PROFILE_RELEASE_OPT_LEVEL=2

prepare() {
	default_prepare

	# shadow git repo for tests
	git init -q

	# Avoid downloading a different toolchain on systems with rustup installed.
	rm rust-toolchain.toml

	cargo fetch --locked
}

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--config-json '{"build-args": "--frozen"}' \
		--output-fd 3 3>&1 >&2

	./target/release/ruff --generate-shell-completion bash > $pkgname.bash
	./target/release/ruff --generate-shell-completion fish > $pkgname.fish
	./target/release/ruff --generate-shell-completion zsh > $pkgname.zsh

	# Update ruff.schema.json as the pre-built one is generated
	# using the '--all-features' Cargo flag which we don't pass.
	cargo dev generate-all
}

check() {
	unset CI_PROJECT_DIR

	cargo test --frozen
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl

	install -Dm644 $pkgname.bash \
		"$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm644 $pkgname.fish \
		"$pkgdir"/usr/share/fish/vendor_completions.d/$pkgname.fish
	install -Dm644 $pkgname.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
a2376a03d029c4b5fbf5f0102262b5da978aaa00874c5f35930cffa0d05f74f116ab87430aeca57ffabd6e821b054a48655d5dc638ba5f6fd392f6cf77d978b7  ruff-0.3.5.tar.gz
"
